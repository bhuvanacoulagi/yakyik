class AddRequirementsToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :requirement, :text
  end
end
