class CreateContributions < ActiveRecord::Migration
  def change
    create_table :contributions do |t|
      t.references :contributor, polymorphic: true, index: true
      t.references :contributed, polymorphic: true, index: true
      t.string :charity
      t.integer :amount

      t.timestamps null: false
    end
  end
end
