require 'rails_helper'

RSpec.describe "vote_comments/new", type: :view do
  before(:each) do
    assign(:vote_comment, VoteComment.new(
      :user_id => 1,
      :comment_id => 1
    ))
  end

  it "renders new vote_comment form" do
    render

    assert_select "form[action=?][method=?]", vote_comments_path, "post" do

      assert_select "input#vote_comment_user_id[name=?]", "vote_comment[user_id]"

      assert_select "input#vote_comment_comment_id[name=?]", "vote_comment[comment_id]"
    end
  end
end
