class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :accepts, as: :acceptor
  has_many :accepted, :through => :accepts, :source => :accepted, source_type: "Challenge"
  has_many :comments
  has_many :challenges, as: :creator
  has_many :contributed, :through => :contributions, :source => :contributed, source_type: "Challenge"
  has_many :contributions, as: :contributor
  has_many :groups, through: :user_groups
  has_many :user_groups
  has_many :vote_challenges
  has_many :vote_comments

end
