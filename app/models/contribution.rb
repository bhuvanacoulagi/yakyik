class Contribution < ActiveRecord::Base

  belongs_to :contributed, polymorphic: true
  belongs_to :contributor, polymorphic: true

end
